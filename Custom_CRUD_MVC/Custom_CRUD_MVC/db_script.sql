USE [employee_management_system]
GO
/****** Object:  Table [dbo].[categories]    Script Date: 10/5/2022 6:39:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[categories](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[category_name] [varchar](100) NULL,
	[status] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[products]    Script Date: 10/5/2022 6:39:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[products](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[product_name] [varchar](100) NULL,
	[product_price] [int] NULL,
	[product_quantity] [int] NULL,
	[brand_name] [varchar](100) NULL,
	[category_id] [int] NULL,
	[status] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[users]    Script Date: 10/5/2022 6:39:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[users](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[first_name] [varchar](255) NULL,
	[last_name] [varchar](255) NULL,
	[email] [varchar](255) NULL,
	[password] [varchar](255) NULL,
	[address] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[categories] ON 

INSERT [dbo].[categories] ([id], [category_name], [status]) VALUES (1, N'Apparel', 1)
INSERT [dbo].[categories] ([id], [category_name], [status]) VALUES (2, N'Shoes', 0)
INSERT [dbo].[categories] ([id], [category_name], [status]) VALUES (3, N'', 1)
INSERT [dbo].[categories] ([id], [category_name], [status]) VALUES (4, N'Check', 1)
INSERT [dbo].[categories] ([id], [category_name], [status]) VALUES (5, N'aaaa', 1)
INSERT [dbo].[categories] ([id], [category_name], [status]) VALUES (6, N'sdfsdf', 1)
INSERT [dbo].[categories] ([id], [category_name], [status]) VALUES (7, N'Watch', 1)
SET IDENTITY_INSERT [dbo].[categories] OFF
GO
SET IDENTITY_INSERT [dbo].[users] ON 

INSERT [dbo].[users] ([id], [first_name], [last_name], [email], [password], [address]) VALUES (1, N'Lee', N'Ray', N'xajydec@mailinator.com', N'Pa$$w0rd!', N'Consequuntur non exe')
INSERT [dbo].[users] ([id], [first_name], [last_name], [email], [password], [address]) VALUES (2, N'Slade', N'Atkinson', N'hivije@mailinator.com', N'Pa$$w0rd!', N'Facere id vel nulla')
INSERT [dbo].[users] ([id], [first_name], [last_name], [email], [password], [address]) VALUES (3, N'Stephanie', N'Bradshaw', N'nyhumukedi@mailinator.com', N'Pa$$w0rd!', N'Sunt et fugiat obca')
INSERT [dbo].[users] ([id], [first_name], [last_name], [email], [password], [address]) VALUES (4, N'Lucian', N'Gibbs', N'viziqujyp@mailinator.com', N'Pa$$w0rd!', N'Occaecat qui nulla q')
INSERT [dbo].[users] ([id], [first_name], [last_name], [email], [password], [address]) VALUES (5, N'Brenden', N'Hudson', N'xajar@mailinator.com', N'Pa$$w0rd!', N'Do est debitis sed a')
SET IDENTITY_INSERT [dbo].[users] OFF
GO
ALTER TABLE [dbo].[products]  WITH CHECK ADD FOREIGN KEY([category_id])
REFERENCES [dbo].[categories] ([id])
GO
