﻿using Custom_CRUD_MVC.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Custom_CRUD_MVC.Controllers
{
    public class HomeController : Controller
    {
        public string cs = ConfigurationManager.ConnectionStrings["ems"].ConnectionString;

        // GET: Home
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Login()
        {
            if (Session["user_id"] == null)
            {
                return View();
            }
            else
            {
                return RedirectToRoute(new
                {
                    controller = "User",
                    action = "ViewUser",
                });
            }
        }

        [HttpPost]
        public ActionResult Login(User user)
        {
            SqlConnection connection = new SqlConnection(cs);
            string query = "select * from users where email = '" + user.email + "' AND password = '" + user.password + "'";
            SqlCommand cmd = new SqlCommand(query, connection);
            connection.Open();
            SqlDataReader sdr = cmd.ExecuteReader();
            if (sdr.HasRows != false)
            {
                sdr.Read();
                Session["user_id"] = sdr["id"];
                Response.Write("<script>console.log('Successfully Login...')</script>");
                return RedirectToRoute(new
                {
                    controller = "User",
                    action = "ViewUser",
                });
            }
            else
            {
                Response.Write("<script>console.log('Invcalid Credentials !!!')</script>");
            }
            return View();
        }
    }
}