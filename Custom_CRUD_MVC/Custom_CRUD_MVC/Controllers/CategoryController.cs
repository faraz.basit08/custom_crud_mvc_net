﻿using Custom_CRUD_MVC.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Custom_CRUD_MVC.Controllers
{
    public class CategoryController : Controller
    {
        public string cs = ConfigurationManager.ConnectionStrings["ems"].ConnectionString;
        // GET: User
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult CreateCategory()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateCategory(Category cat)
        {
            SqlConnection connection = new SqlConnection(cs);
            connection.Open();
            string query = "INSERT INTO categories (category_name,status)" +
                " VALUES ('" + cat.category_name + "',1)";
            SqlCommand cmd = new SqlCommand(query, connection);
            cmd.ExecuteNonQuery();
            connection.Close();

            return RedirectToAction("ViewCategory");
        }

        public ActionResult EditCategory(int id)
        {
            SqlConnection connection = new SqlConnection(cs);
            string query = "SELECT * FROM users where id = '"+ id + "'";
            SqlCommand cmd = new SqlCommand(query, connection);
            var modelList = new List<User>();
            connection.Open();
            SqlDataReader sdr = cmd.ExecuteReader();
            while (sdr.Read())
            {
                var user = new User();
                user.id = (int)sdr["id"];
                user.first_name = (string)sdr["first_name"];
                user.last_name = (string)sdr["last_name"];
                user.email = (string)sdr["email"];
                user.password = (string)sdr["password"];
                user.address = (string)sdr["address"];
                modelList.Add(user);
            }
            connection.Close();
            return View(modelList);
        }



        public ActionResult ViewCategory()
        {
            SqlConnection connection = new SqlConnection(cs);
            string query = "SELECT * FROM categories";
            SqlCommand cmd = new SqlCommand(query, connection);
            var modelList = new List<Category>();
            connection.Open();
            SqlDataReader sdr = cmd.ExecuteReader();
            while (sdr.Read())
            {
                var cat = new Category();
                cat.id = (int)sdr["id"];
                cat.category_name = (string)sdr["category_name"];
                cat.status = (int)sdr["status"];
                modelList.Add(cat);
            }
            connection.Close();
            return View(modelList);
        }
    }
}