﻿using Custom_CRUD_MVC.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Custom_CRUD_MVC.Controllers
{
    public class UserController : Controller
    {
        public string cs = ConfigurationManager.ConnectionStrings["ems"].ConnectionString;
        // GET: User
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult CreateUser()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateUser(User user)
        {
            SqlConnection connection = new SqlConnection(cs);
            connection.Open();
            string query = "INSERT INTO users (first_name,last_name,email,password,address)" +
                " VALUES ('" + user.first_name+ "','" + user.last_name + "','" + user.email + "','" + user.password + "','" + user.address + "')";
            SqlCommand cmd = new SqlCommand(query, connection);
            cmd.ExecuteNonQuery();
            connection.Close();

            return View();
        }

        public ActionResult EditUser(int id)
        {
            SqlConnection connection = new SqlConnection(cs);
            string query = "SELECT * FROM users where id = '"+ id + "'";
            SqlCommand cmd = new SqlCommand(query, connection);
            var modelList = new List<User>();
            connection.Open();
            SqlDataReader sdr = cmd.ExecuteReader();
            while (sdr.Read())
            {
                var user = new User();
                user.id = (int)sdr["id"];
                user.first_name = (string)sdr["first_name"];
                user.last_name = (string)sdr["last_name"];
                user.email = (string)sdr["email"];
                user.password = (string)sdr["password"];
                user.address = (string)sdr["address"];
                modelList.Add(user);
            }
            connection.Close();
            return View(modelList);
        }



        public ActionResult ViewUser()
        {
            if (Session["user_id"] != null)
            {
                SqlConnection connection = new SqlConnection(cs);
                string query = "SELECT * FROM users";
                SqlCommand cmd = new SqlCommand(query, connection);
                var modelList = new List<User>();
                connection.Open();
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    var user = new User();
                    user.id = (int)sdr["id"];
                    user.first_name = (string)sdr["first_name"];
                    user.last_name = (string)sdr["last_name"];
                    user.email = (string)sdr["email"];
                    user.password = (string)sdr["password"];
                    user.address = (string)sdr["address"];
                    modelList.Add(user);
                }
                connection.Close();
                return View(modelList);
            }
            else
            {
                return RedirectToRoute(new
                {
                    controller = "Home",
                    action = "Login",
                });
            }
        }
    }
}