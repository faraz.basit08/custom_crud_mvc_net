﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Custom_CRUD_MVC.Models
{
    public class User
    {
        public int id { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        public string address { get; set; }
    }
}