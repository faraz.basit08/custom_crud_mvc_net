﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Custom_CRUD_MVC.Models
{
    public class Products
    {
        public int id { get; set; }
        public string product_name { get; set; }
        public int product_price { get; set; }
        public int product_quantity { get; set; }
        public string brand_name { get; set; }
        public int status { get; set; }
        public int category_id { get; set; }

    }
}