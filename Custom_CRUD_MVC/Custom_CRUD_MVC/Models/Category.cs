﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Custom_CRUD_MVC.Models
{
    public class Category
    {
        public int id { get; set; }
        public string category_name { get; set; }
        public int status { get; set; }

    }
}